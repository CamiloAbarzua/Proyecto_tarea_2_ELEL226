//Variables.
int vel;
float x,y;
int radius;

void setup(){
size(1200,600);
ellipseMode(RADIUS);
x=10;
y=200;
radius=15;
vel=0;
}

void draw(){
fondo();
boton1();
boton2();
flecha();
x+=vel;
}

void fondo(){
background(#FFFFFF);
stroke(#030303);
strokeWeight(2);
fill(#B45688);  
rect(0,0,width,100);

stroke(#030303);
strokeWeight(2);
fill(#B45688);  
rect(0,300,width,300);
}
void flecha(){
 beginShape();
   stroke(#030303);
   strokeWeight(3);
   fill(#CE361B);
   vertex(x,y);
   vertex(x+50,y);
   vertex(x+50,y-5);
   vertex(x+100,y+5);
   vertex(x+50,y+15);
   vertex(x+50,y+10);
   vertex(x,y+10);
   vertex(x,y);
 endShape();
}
void boton1(){
  float d = dist(mouseX,mouseY,100,380);
  if(d<radius){
    fill(0);
    vel=vel+1;
    ellipse(100,380,radius,radius);
  }else{
    fill(255);
    if (vel>0.1){
    vel=vel-1;
    }else{
      vel=0;
  }
  ellipse(100,380,radius,radius);
}
}
void boton2(){
  float d = dist(mouseX,mouseY,200,380);
  if(d<radius){
    fill(0);
    if (vel>0){
    vel=vel-1;
    }else{
    vel=0;}
  }else{
    fill(255);
  }
  ellipse(200,380,radius,radius);
}
