//Import
import meter.*;

Meter m;
//Variables.
int vel;
float x,y;
int radius;

void setup(){
size(1200,600);
ellipseMode(RADIUS);
x=10;
y=200;
radius=15;
vel=0;
}

void draw(){
fondo();
medidor();
boton1();
boton2();
flecha();
x+=vel;
}

void fondo(){
background(#FFFFFF);
stroke(#030303);
strokeWeight(2);
fill(#B45688);  
rect(0,0,width,100);

stroke(#030303);
strokeWeight(2);
fill(#B45688);  
rect(0,300,width,300);
}
void flecha(){
 beginShape();
   stroke(#030303);
   strokeWeight(3);
   fill(#CE361B);
   vertex(x,y);
   vertex(x+50,y);
   vertex(x+50,y-5);
   vertex(x+100,y+5);
   vertex(x+50,y+15);
   vertex(x+50,y+10);
   vertex(x,y+10);
   vertex(x,y);
 endShape();
}

void boton1(){
  float d = dist(mouseX,mouseY,100,380);
  if(d<radius){
    fill(0);
    vel=vel+1;
    ellipse(100,380,radius,radius);
  }else{
    fill(255);
    if (vel>0.1){
    vel=vel-1;
    }else{
      vel=0;
  }
  ellipse(100,380,radius,radius);
}
}
void boton2(){
  float d = dist(mouseX,mouseY,200,380);
  if(d<radius){
    fill(0);
    if (vel>0){
    vel=vel-1;
    }else{
    vel=0;}
  }else{
    fill(255);
  }
  ellipse(200,380,radius,radius);
}


 void medidor(){
 m = new Meter(this,700,325);
 m.setTitleFontSize(20);
 m.setTitleFontName("Arial bold");
 m.setTitle("Velocidad");
 
 //Escala
 String[] scaleLabels = {"0", "0.2","0.4","0.6","0.8","1.0","1.2","1.4", "1.6","1.8","2.0","2.2","2.4"};
 m.setMinScaleValue(0.0);
 m.setMaxScaleValue(2.5);
 m.setScaleLabels(scaleLabels);
 m.setScaleFontSize(18);
 m.setScaleFontColor(#5907F5);
 
 m.setDisplayDigitalMeterValue(true);
 int value = vel;
 m.updateMeter(value);
}
