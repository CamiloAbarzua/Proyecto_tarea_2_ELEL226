//Variables.
float vel;
float x,y;

void setup(){
size(1200,600);
ellipseMode(RADIUS);
x=10;
y=200;
vel=1;
}

void draw(){
fondo();
flecha();
x+=vel;
}

void fondo(){
background(#FFFFFF);
stroke(#030303);
strokeWeight(2);
fill(#B45688);  
rect(0,0,width,100);

stroke(#030303);
strokeWeight(2);
fill(#B45688);  
rect(0,300,width,300);
}
void flecha(){
 beginShape();
   stroke(#030303);
   strokeWeight(3);
   fill(#CE361B);
   vertex(x,y);
   vertex(x+50,y);
   vertex(x+50,y-5);
   vertex(x+100,y+5);
   vertex(x+50,y+15);
   vertex(x+50,y+10);
   vertex(x,y+10);
   vertex(x,y);
 endShape();
}
