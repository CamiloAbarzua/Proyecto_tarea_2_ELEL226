//Import
import meter.*;

Meter m;
//Variables.
float vel;
float x,y;
int radius;
int vel2;
boolean redlightOn;
int contador;

void setup(){
size(1200,600);
ellipseMode(RADIUS);
x=10;
y=200;
radius=15;
vel=0;
contador=0;
}

void draw(){
fondo();
medidor();
boton1();
boton2();
flecha();
light();
x+=vel;
}

void fondo(){
background(#FFFFFF);
stroke(#030303);
strokeWeight(2);
fill(#B45688);  
rect(0,0,width,100);

stroke(#030303);
strokeWeight(2);
fill(#B45688);  
rect(0,300,width,300);
}
void flecha(){
   stroke(#030303);
   strokeWeight(3);
   fill(#CE361B);
   ellipse(x,y,8,8);
}

void boton1(){
  float d = dist(mouseX,mouseY,100,380);
  if(d<radius){
    fill(0);
    ellipse(100,380,radius,radius);
    if(x<1200){
      vel=vel+0.5;
    }else{
      x=0;
       vel=vel-0.5;
    }
  }else{
    fill(255);
    ellipse(100,380,radius,radius);
    if (abs(vel)>0.1){
      if(x<1200){
        vel=vel-0.5;
      }else{
        x=0;
        vel=vel-0.5;
    }
    }else{
      vel=0;
    }
  
  ellipse(100,380,radius,radius);
}
}
void boton2(){
  float d = dist(mouseX,mouseY,200,380);
  if(d<radius){
    redlightOn = true;
    fill(0);
    if (abs(vel)>0){
    vel=vel-0.5;
    }else{
    vel=0;}
  }else{
    redlightOn = false;
    fill(255);
  }
  ellipse(200,380,radius,radius);
}


 void medidor(){
 m = new Meter(this,700,325);
 m.setTitleFontSize(20);
 m.setTitleFontName("Arial bold");
 m.setTitle("Velocidad");
 
 //Escala
 String[] scaleLabels = {"0", "0.2","0.4","0.6","0.8","1.0","1.2","1.4", "1.6","1.8","2.0","2.2","2.4"};
 m.setMinScaleValue(0.0);
 m.setMaxScaleValue(2.5);
 m.setScaleLabels(scaleLabels);
 m.setScaleFontSize(18);
 m.setScaleFontColor(#5907F5);
 
 m.setDisplayDigitalMeterValue(true);
 vel2=int(vel);
 int value = abs(vel2);
 m.updateMeter(value);
}

void light(){
  if (redlightOn == true){
    fill(#FF0303);
    ellipse(40,40,10,10);
    textSize(20);
    fill(0);
    text("Frenando",60,48);
  }else{
  noFill();
  ellipse(40,40,10,10);
}
}


  
  
  
